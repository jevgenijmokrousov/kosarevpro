/**
 * @author info@74dpi.com
 * @todo
 *
 */

var App = window.App = window.App || {};

App.Common = {

    params : {},

    init : function(params) {
        var self = this;

        self.params = $.extend(self.params, params);

        self.initTitlePosition($("._jsTitle"));

    },

    initTitlePosition : function($e) {
        var self = this;
        
        $e.each(function(){
            var t = $(this);
            var width  = t.width();
            var height = t.height();
            
            console.log(height);
            
            t.css({"marginTop": "-" + Math.floor(height/2) + "px", "marginLeft": "-" + Math.floor(width/2) + "px"}); 
        });
        
    }
};

$(document).ready(function() {

    App.Common.init({});

});
